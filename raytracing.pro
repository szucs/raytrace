;
; FUNCTION single_ray_trace()
;
; This function does the actual raytracing along a single ray, given by the 
; HEALpix function in the main code. The main input parameters are as follows:
;
;    vector     3 element vector containing the x, y, z normalized components of 
;               the current ray
;    density    3D array containing the number density distribution
;    lstep      step size in the unit of the pixel size, by reducing this the 
;               computation can be speeded up
;    x0,y0,z0   coordinates of the current pixel in cm
;    dx,dz,dy   pixel sizes in cm
;    xmax,..    x, y, z coordinates of the box baundary in the increasing coordinate
;               direction
;    ymin,..    the same as the one before, but to the decreasing direction
;    maxstep    after taking this many steps, the code terminates, even if the 
;               box baundary is not reached.
;
; Generally the FORTRAN implementation follows the same algorithm as this.
;
FUNCTION single_ray_trace, vector, density, lstep, x0, y0, z0, dx, dy, dz, xmax, ymax, zmax, $
             xmin, ymin, zmin, maxstep=maxstep, verbose=verbose
    xcurrent = x0 & ycurrent = y0 & zcurrent = z0
    vx = vector[0] & vy = vector[1] & vz = vector[2]

    nstep = 0l
    colray = 0.0d0

    xstep = dx * vx * lstep
    ystep = dy * vy * lstep
    zstep = dz * vz * lstep

; distance walked in this step
    abslength = sqrt(xstep^2. + ystep^2. + zstep^2.)

    xcurrent = (nstep * xstep) + x0
    ycurrent = (nstep * ystep) + y0
    zcurrent = (nstep * zstep) + z0

; Trace the ray, while we are inside the box
    WHILE (xcurrent LT xmax) AND (xcurrent GE xmin) AND $
             (ycurrent LT ymax) AND (ycurrent GE ymin) AND $
             (zcurrent LT zmax) AND (zcurrent GE zmin) DO BEGIN

; current x, y, z indexes
       xindex = fix((xcurrent - xmin) / dx)
       yindex = fix((ycurrent - ymin) / dy)
       zindex = fix((zcurrent - zmin) / dz)

       if keyword_set(verbose) then begin
         print, nstep, xindex, yindex, zindex, density[xindex,yindex,zindex]
;          print, nstep, vector[0], vector[1], vector[2]
       endif

; Some precotion not to leave the box by mistake
       if zindex eq 256 then print, zindex, zcurrent, zmax
       if zindex eq -1 then print, zindex, zcurrent, zmin

       IF ( abs(xcurrent-x0) GT (dx * 0.5) ) OR ( abs(ycurrent-y0) GT (dy * 0.5) ) $
          OR ( abs(zcurrent-z0) GT (dz * 0.5) ) THEN BEGIN 
          colray = colray + ( density[xindex,yindex,zindex] * abslength )
       ENDIF 

; advance to the next step
       nstep = nstep + 1

       xcurrent = (nstep * xstep) + x0
       ycurrent = (nstep * ystep) + y0
       zcurrent = (nstep * zstep) + z0

       IF nstep gt maxstep then begin
          PRINT, 'WARNING MAXSTEP = ',maxstep,' is reached in ', x0, y0, z0,' STOPPING!'
          STOP
       ENDIF

    ENDWHILE

; return the column density along the ray, the number of steps walked and the 
; total distance walked in this direction.
    RETURN, {colray:colray,nstep:nstep,l:abslength*nstep}
END

;
; FUNCTION avg_av(av_array)
;
; This function averages the Av distribution over 4 pi. This is not a unique way 
; for averaging the Av over the sky, since it depends on an factor (-2.5):
; <Av> = alog( sum( exp( -2.5 * Avarray[i] )  ) / npix ) / -2.5
; The factor here is from the CO dust shielding, which is prop. to exp(-2.5 * Av)
; 
; With this expression we basically calculate an effective Av: if use this to calculate
; the CO shielding, then we would get the same value as if all the direction would 
; have been considered.
;
; However, since the factor in the shielding expression depends from species to 
; species, this average does not give back the same effective value, as the whole 
; distribution of Av.
; 
; It might not be a good choice either when the Av is considered for other purposes 
; than the shielding from the UV radiation.

FUNCTION avg_av, avarray

@natconst.pro

npix = n_elements(avarray)
av_mean = alog(sumarr(exp(-2.5d0*avarray)) / double(npix)) / (-2.5d0)

RETURN, av_mean

END

;
; PROCEDURE raytracing using HEALPIX
;
; This program requires the HEALPix IDL library, which can be found at:
; 
; http://healpix.jpl.nasa.gov/html/idl.htm         (this is important!!)
;
; Two modes are available: one purely IDL (very slow) and one which calls
; a FORTRAN shared library to do the raytracing (pretty fast). For the latter
; to work, the shared library path must be given using the RAYTRACE_FORT_PATH 
; variable.
; 
; The input structure (box) should be a radmc3d idl structure
;
; 

@cgprogressbar__define

PRO raytracing, box, result, npix=npix,lstep=lstep,maxstep=maxstep,test=test, $
         nxnynz=nxnynz, ps=ps, fortran=fortran
; Get and set some important constants and parameter values:
@natconst.pro
RAYTRACE_FORT_PATH = ' /home/laszlo/Work/sources/raytracing/'

; get the box dimensions
nx = box.ngridx & ny = box.ngridy & nz = box.ngridz
dx = box.x[1]-box.x[0] & dy = box.y[1]-box.y[0] & dz = box.z[1]-box.z[0]
xmin = min(box.x) & xmax = max(box.x)
ymin = min(box.y) & ymax = max(box.y)
zmin = min(box.z) & zmax = max(box.z)

x = box.x[0:-2] + abs(box.x[1:-1] - box.x[0:-2]) * 0.5
y = box.y[0:-2] + abs(box.y[1:-1] - box.y[0:-2]) * 0.5
z = box.z[0:-2] + abs(box.z[1:-1] - box.z[0:-2]) * 0.5

; Convert mass density to number density (of H nucleii). We assume a mean 
; molecular weight of 1.4:
rho = box.data / (1.4*mp)

; initialize the HEALPIX directions
IF NOT KEYWORD_SET(npix) THEN npix = 48
IF NOT KEYWORD_SET(lstep) THEN lstep = 0.2
lstep = double(lstep)
IF NOT KEYWORD_SET(maxstep) THEN maxstep = long64(500000)

nside = npix2nside(npix,error=error)    ; get the number of sides, this is necessary later
IF error EQ 1 THEN BEGIN
   PRINT, 'ERROR: NPIX is not valied, NPIX = 12*NSIDE^2, where NSIDE is a power of 2'
   STOP
ENDIF
rayindex = indgen(npix)
pix2vec_ring, nside, rayindex, rays ; this is the 3d vectors for each rays 
                                          ; normalised to 1

;
; This is the first mode of the code: it calculates the column density that a 
; voxel (3D pixel) sees, that is located in a given 3D density distribution 
; (this is contained in the 'box' variable.
; This mode is good for plotting maps of extinction (with the use of HEALpix 
; functions). For moderate pixel numbers it is not necessary to use the FORTRAN
; code, since the IDL function is fast enough.
;
IF KEYWORD_SET(test) THEN BEGIN
; choose a pixel for testing:
   IF NOT KEYWORD_SET(nxnynz) THEN nxnynz = [15,20,21]
   x0 = x[nxnynz[0]] & y0 = y[nxnynz[1]] & z0 = z[nxnynz[2]]
   total_column_density = dblarr(npix)
   nstep_array = indgen(npix)
   length_traveled = dblarr(npix)

; The local cuntribution, first the full, then that of from mass outside of the 
; dx * 0.25 radius circle around the cell center.
   local_Av = 0.25 * dx * rho[nxnynz[0],nxnynz[1],nxnynz[2]] * 5.34788e-22

   IF KEYWORD_SET(fortran) THEN BEGIN

      colray = dblarr(npix)
      vector = rays

; make a symbolic link in our current directiory to the shared lib.
      spawn,'ln -s ' + RAYTRACE_FORT_PATH + 'single_ray_trace.so.1.0 .'
      spawn,'ln -s ' + RAYTRACE_FORT_PATH + 'single_ray_trace.o .'

      print, rho[nxnynz[0],nxnynz[1],nxnynz[2]],lstep,x0,y0,z0,dx,dy,dz,xmax,ymax,zmax, xmin,ymin,zmin, $
             long(maxstep),long(nx),long(ny),long(nz),long(npix)
      print, vector[5,*]
      S= CALL_EXTERNAL('single_ray_trace.so.1.0', 'single_ray_trace', $
          vector,rho,lstep,x0,y0,z0,dx,dy,dz,xmax,ymax,zmax,$
           xmin,ymin,zmin,long(maxstep),long(nx),long(ny),long(nz),long(npix),colray)

       total_column_density = colray
       nstep_array = colray*0
       length_traveled = colray*0

; clean up the symbolic links:
      spawn, 'rm single_ray_trace.so.1.0 single_ray_trace.o'

   ENDIF ELSE BEGIN

     FOR i=0, npix-1 do begin
;       if i eq 5 then print, rays[i,*]
       vector = transpose(rays[i,*])
       if i eq 5 then vb=1 else vb=0
;       if i eq 5 then print, vector
       tmp = single_ray_trace(vector,rho,lstep,x0,y0,z0,dx,dy,dz,xmax,ymax,zmax,$
           xmin,ymin,zmin,maxstep=maxstep,verbose=vb)
       total_column_density[i] = tmp.colray
       nstep_array[i] = tmp.nstep
       length_traveled[i] = tmp.l
     ENDFOR
   ENDELSE

   Av_array = (total_column_density * 5.34788e-22) ; + local_Av ; not needed

; Plot the results to a sphere, using equal area pixels and the HEALpix lib.
   mollview,Av_array,title='Visual extinction [mag]',/silent,units='mag', $
        subtitle= 'Local contribution = '+str(strcompress(local_Av))+' mag',$
        ps=ps

; Calculate the average Av, weighted towards the smaller values
   avg_min_Av = 1. / mean(1./Av_array)
; Calciulate the average <Av> = -1/2.5 * ln(1/npix * sum( exp(-2.5 * Av(1..npix)) ))
   avg_CO_Av = avg_Av(Av_array)
   print, 'Av_mean (weighted towards small) = ', avg_min_Av, ' mag'
   print, 'Av_mean (with CO shielding factor) = ', avg_CO_Av, ' mag'
; Plot more information - optional (remove the comment signs)
   ;mollview,nstep_array, title='Number of steps to box edge'
   ;mollview,length_traveled/pc

;
; IF not in TEST mode: the mean Av will be calculated in each and every grid cells   
; To speed up the calculation, we *highly* recommend the use of the /fortran 
; option.
;
ENDIF ELSE BEGIN

; Possible outputs, currently only the Av_avg is used (others are not implemented
; in the FORTRAN code or take up a lot of memeory in the IDL code)

;total_column_density = dblarr(nx,ny,nz,npix)
;Av_full = dblarr(nx,ny,nz,npix)
Av_avg = dblarr(nx,ny,nz)

;nstep_array = indgen(nx,ny,nz,npix)

; Do the fast calculation with the FORTRAN code -- the shared lib. path must be 
; given (see in the procedure description)
IF KEYWORD_SET(fortran) THEN BEGIN

   colray = dblarr(npix)
   vector = rays
   lmin = dblarr(nx,ny,nz)

; make a symbolic link in our current directiory to the shared lib.
   spawn,'ln -s ' + RAYTRACE_FORT_PATH + 'single_ray_trace.so.1.0 .'
   spawn,'ln -s ' + RAYTRACE_FORT_PATH + 'single_ray_trace.o .'

   S= CALL_EXTERNAL('single_ray_trace.so.1.0', 'all_ray_trace', $
        vector,rho,lstep,x,y,z,dx,dy,dz,xmax,ymax,zmax,$
        xmin,ymin,zmin,long(maxstep),long(nx),long(ny),long(nz),long(npix),$
        Av_avg,lmin)

; clean up the symbolic links:
   spawn, 'rm single_ray_trace.so.1.0 single_ray_trace.o'

; If the FORTRAN lib. is not available, the IDL code can be used, but
; WARNING: this is very slow
ENDIF ELSE BEGIN

; Graphical progeress bar:
; Since it takes ages the progress bar is very instructive
; It needs the coyote procedure cgProgressBar:
; http://www.idlcoyote.com/programs/cgprogressbar__define.pro
cgProgressBar = Obj_New("CGPROGRESSBAR");, /Cancel)
cgProgressBar -> Start
nr_total = long(nx * ny * nz)   ; total number of pixels
nr_current = 0l
lmin = dblarr(nx,ny,nz) + 1e4 * pc 

FOR i=0, nx-1 DO BEGIN
  FOR j=0, ny-1 DO BEGIN
    FOR k=0, nz-1 DO BEGIN

      x0 = x[i] & y0 = y[j] & z0 = z[k]   
      avtmp = dblarr(npix)

; A crude approximation of the local Av contribution -- this can be done better!!
      local_Av = 0.25 * dx * rho[i,j,k] * 5.34788e-22  ; actually we don't need this
                                                       ; anymore, the rays are computed
                                                       ; from the cell center, with
                                                       ; smaller step length than the 
                                                       ; cell size

      FOR m=0, npix-1 do begin
        vector = transpose(rays[m,*])
        tmp = single_ray_trace(vector,rho,lstep,x0,y0,z0,dx,dy,dz,xmax,ymax,zmax,$
           xmin,ymin,zmin,maxstep=maxstep)
        avtmp[m] = (tmp.colray * 5.34788e-22) ; + local_Av ; not needed, see a bit above
;       total_column_density[i,j,k,m] = tmp.colray
;        nstep_array[i,j,k,m] = tmp.nstep
        IF tmp.l lt lmin[i,j,k] THEN lmin[i,j,k] = tmp.l
      ENDFOR

      Av_avg[i,j,k] = avg_av(avtmp) ; total(avtmp) / double(npix) ;  avg_av(avtmp) ; 1. / mean(1. / avtmp)

      nr_current = nr_current + 1l
; Update the progress bar
      cgProgressBar -> Update, (double(nr_current)/double(nr_total) * 100.)

    ENDFOR
  ENDFOR
ENDFOR
; Close the progress bar
cgProgressBar -> Destroy

ENDELSE

; Save results to an IDL save file
result = {NGRIDX: nx, NGRIDY: ny, NGRIDZ: nz, X: box.x, Y: box.y, Z: box.z, $
          Av_avg: Av_avg, LMIN: lmin}

save, result, filename='Av_raytrace_avg.sav'

ENDELSE

END

