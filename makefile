single_ray_trace.so:	      raytrace.o single_ray_trace.o
	      gcc -shared -Wl,-soname,single_ray_trace.so.1 -o single_ray_trace.so.1.0 raytrace.o single_ray_trace.o -lc
single_ray_trace.o:	          single_ray_trace.f
	      gfortran -shared -fopenmp -fPIC single_ray_trace.f -o single_ray_trace.o
raytrace.o:	          raytrace.c
	      gcc -c -fPIC raytrace.c -o raytrace.o
clean:
	      rm -f single_ray_trace.o raytrace.o single_ray_trace.so.1 single_ray_trace.so.1.0
