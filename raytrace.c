    #include <stdio.h>
    #include "export.h"

    void single_ray_trace(int argc, void *argv[])
    {
     extern void single_ray_trace_(); /* Fortran routine */
// test
/*      double *a, *b, *c, *d, *zz;
      IDL_INT *nz;
      
      a = (double *) argv[0];
      b = (double *) argv[1];
      c = (double *) argv[2];
      d = (double *) argv[3];
      nz = (IDL_INT *) argv[4];
      zz = (double *) argv[5];

      makegrid_(a,b,c,d,nz,zz);*/

// Double variables     
     double *lstep,*x0,*y0,*z0,*dx,*dy,*dz;
     double *xmax, *ymax, *zmax, *xmin, *ymin, *zmin, *colray;
// Arrays     
     double *vector, *density;
// Integers     
     int *maxstep, *nx, *ny, *nz, *nray;



     vector = (double *) argv[0]; /* Array pointer */
     density = (double *) argv[1]; /* Array pointer */
     lstep = (double *) argv[2]; /* Array pointer */
     x0 = (double *) argv[3]; /* Array pointer */
     y0 = (double *) argv[4]; /* Array pointer */
     z0 = (double *) argv[5]; /* Array pointer */
     dx = (double *) argv[6]; /* Array pointer */
     dy = (double *) argv[7]; /* Array pointer */
     dz = (double *) argv[8]; /* Array pointer */
     xmax = (double *) argv[9]; /* Array pointer */
     ymax = (double *) argv[10]; /* Array pointer */
     zmax = (double *) argv[11]; /* Array pointer */
     xmin = (double *) argv[12]; /* Array pointer */
     ymin = (double *) argv[13]; /* Array pointer */
     zmin = (double *) argv[14]; /* Array pointer */
     maxstep = (int *) argv[15]; /* Array pointer */
     nx = (int *) argv[16]; /* Array pointer */
     ny = (int *) argv[17]; /* Array pointer */
     nz = (int *) argv[18]; /* Array pointer */
     nray = (int *) argv[19]; /* Array pointer */
     colray = (double *) argv[20]; /* Array pointer */

//     printf("\n %e \n", lstep);
//     printf("\n %e,%e,%e  \n", x0[0], xmax[0], xmin[0]);
//     printf("\n %i,%i,%i  \n", maxstep[0], nx[0], ny[0]);
// Do the calculations in Fortran
     single_ray_trace_(vector, density, lstep, x0, y0, z0, dx, dy, dz, xmax, 
             ymax, zmax, xmin, ymin, zmin, maxstep, nx, ny, nz, nray, colray);
    }


    void all_ray_trace(int argc, void *argv[])
    {
     extern void all_ray_trace_(); /* Fortran routine */
// test
/*      double *a, *b, *c, *d, *zz;
      IDL_INT *nz;
      
      a = (double *) argv[0];
      b = (double *) argv[1];
      c = (double *) argv[2];
      d = (double *) argv[3];
      nz = (IDL_INT *) argv[4];
      zz = (double *) argv[5];

      makegrid_(a,b,c,d,nz,zz);*/

// Double variables     
     double *lstep,*x,*y,*z,*dx,*dy,*dz;
     double *xmax, *ymax, *zmax, *xmin, *ymin, *zmin;
// Arrays     
     double *vector, *density, *avg_av, *lmin;
// Integers     
     int *maxstep, *nx, *ny, *nz, *nray;



     vector = (double *) argv[0]; /* Array pointer */
     density = (double *) argv[1]; /* Array pointer */
     lstep = (double *) argv[2]; /* Array pointer */
     x = (double *) argv[3]; /* Array pointer */
     y = (double *) argv[4]; /* Array pointer */
     z = (double *) argv[5]; /* Array pointer */
     dx = (double *) argv[6]; /* Array pointer */
     dy = (double *) argv[7]; /* Array pointer */
     dz = (double *) argv[8]; /* Array pointer */
     xmax = (double *) argv[9]; /* Array pointer */
     ymax = (double *) argv[10]; /* Array pointer */
     zmax = (double *) argv[11]; /* Array pointer */
     xmin = (double *) argv[12]; /* Array pointer */
     ymin = (double *) argv[13]; /* Array pointer */
     zmin = (double *) argv[14]; /* Array pointer */
     maxstep = (int *) argv[15]; /* Array pointer */
     nx = (int *) argv[16]; /* Array pointer */
     ny = (int *) argv[17]; /* Array pointer */
     nz = (int *) argv[18]; /* Array pointer */
     nray = (int *) argv[19]; /* Array pointer */
     avg_av = (double *) argv[20]; /* Array pointer */
     lmin = (double *) argv[21]; /* Array pointer */


//     printf("\n %e \n", lstep);
//     printf("\n %e,%e,%e  \n", x0[0], xmax[0], xmin[0]);
//     printf("\n %i,%i,%i  \n", maxstep[0], nx[0], ny[0]);
// Do the calculations in Fortran
     all_ray_trace_(vector, density, lstep, x, y, z, dx, dy, dz, xmax, 
             ymax, zmax, xmin, ymin, zmin, maxstep, nx, ny, nz, nray, avg_av,
             lmin);
    }
